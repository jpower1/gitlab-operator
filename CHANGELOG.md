## 0.12.0 (2022-09-22)

### fixed (1 change)

- [Add fixes from manual run of 0.10.2 certification](gitlab-org/cloud-native/gitlab-operator@7c0368f587c166bb82870e4a33cda9c7ed0eefb9) ([merge request](gitlab-org/cloud-native/gitlab-operator!511))

### performance (1 change)

- [Add `jobSucceeded` method to check Job status](gitlab-org/cloud-native/gitlab-operator@d6c37cf6ed736777a94ac2519c51dcdbac704e49) ([merge request](gitlab-org/cloud-native/gitlab-operator!503))

### other (1 change)

- [Remove NGINX DefaultBackend from tests](gitlab-org/cloud-native/gitlab-operator@1604313afffe233ae427a2092e704647b7bf8f6d) ([merge request](gitlab-org/cloud-native/gitlab-operator!514))

## 0.11.4 (2022-09-05)

No changes.

## 0.11.3 (2022-08-30)

No changes.

## 0.11.2 (2022-08-23)

No changes.

## 0.11.1 (2022-08-22)

No changes.

## 0.11.0 (2022-08-22)

### security (1 change)

- [Add separate nonroot and anyuid RBAC](gitlab-org/cloud-native/gitlab-operator@01d49a714d62cf8d38220e707edc69f9f71a17ce) ([merge request](gitlab-org/cloud-native/gitlab-operator!447))

### added (3 changes)

- [Add Vale configuration and style references](gitlab-org/cloud-native/gitlab-operator@1546a091cd5ad38166314ffb7cc0cdd22df2ff96) ([merge request](gitlab-org/cloud-native/gitlab-operator!509))
- [Script and document RedHat certification process](gitlab-org/cloud-native/gitlab-operator@cdd3b1ed180434e88054079391ca0d0965ccf0f8) ([merge request](gitlab-org/cloud-native/gitlab-operator!494))
- [Add GKE 1.22 jobs](gitlab-org/cloud-native/gitlab-operator@ecdc70c91cf9f14a1eb3dab68135428c2316de69) ([merge request](gitlab-org/cloud-native/gitlab-operator!497))

### changed (1 change)

- [Use project token for RH certification jobs](gitlab-org/cloud-native/gitlab-operator@3cde0d00e1051a306850b102b3b62bd31af7c34a) ([merge request](gitlab-org/cloud-native/gitlab-operator!505))

### fixed (1 change)

- [Deep copy Chart values for catalog query](gitlab-org/cloud-native/gitlab-operator@9b231838685be534e68d40aab69a30cd1970e5c8) ([merge request](gitlab-org/cloud-native/gitlab-operator!499))

## 0.10.1 (2022-07-28)

No changes.

## 0.10.0 (2022-07-22)

### other (1 change)

- [Add .task/ to gitignore](gitlab-org/cloud-native/gitlab-operator@318e1a386eca5970960e166dcab053b1efab9b26) ([merge request](gitlab-org/cloud-native/gitlab-operator!481))

## 0.9.3 (2022-07-19)

No changes.

## 0.9.2 (2022-07-05)

No changes.

## 0.9.1 (2022-06-30)

No changes.

## 0.9.0 (2022-06-22)

No changes.

## 0.8.2 (2022-06-16)

No changes.

## 0.8.1 (2022-06-01)

No changes.

## 0.8.0 (2022-05-22)

No changes.

## 0.7.2 (2022-05-05)

No changes.

## 0.7.1 (2022-05-02)

No changes.

## 0.7.0 (2022-04-22)

No changes.
